FROM jboss/base-jdk:8
LABEL maintainer="Greg Farr"

ENV JBOSS_ROOT /usr/local/jboss
EXPOSE 8080

# User root user to install software
USER root

# Install unzip
RUN yum install -y unzip

# Install JBOSS EAP
WORKDIR $JBOSS_ROOT
COPY ["./jboss-eap-*.zip", "./"]

RUN unzip -q jboss-eap-7.2.0.zip \
    && chown -R jboss:0 ${JBOSS_ROOT} \
    && chmod -R g+rw ${JBOSS_ROOT}

ENV JBOSS_HOME /usr/local/jboss/jboss-eap-7.2
WORKDIR $JBOSS_HOME

# Ensure signals are forwarded to the JVM process correctly for graceful shutdown
ENV LAUNCH_JBOSS_IN_BACKGROUND true

# Switch back to jboss user
USER jboss
